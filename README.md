sd-BeanCounters-plugin
==============================

Plugin requires sudo access, so please follow ServerDensity knowledgebase http://support.serverdensity.com/knowledgebase/articles/112413-plugins-requiring-sudo
and add
sd-agent ALL=(ALL) NOPASSWD: /bin/cat

If you want to add more parameters to monitor, please reffer to output of sudo cat /proc/user_beancounters and overide default parameter list with your new one
plugin_beancounters_params: 'kmemsize,lockedpages,privvmpages,shmpages,numproc,tcpsndbuf,tcprcvbuf,numfile'

Please note that this list 'kmemsize,lockedpages,privvmpages,shmpages,numproc,tcpsndbuf,tcprcvbuf,numfile' is by default, you need to specify whole list including your parameters, don't just put your addtional parameters. As you can see, you can reduce this list by just overrding it via configuration file.


