"""Server Density plugin for monitoring status of /proc/user_beancounters

Copyright 2011  Artur Reznikov areznikov@live.com. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are
permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of
conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list
of conditions and the following disclaimer in the documentation and/or other materials
provided with the distribution.

THIS SOFTWARE IS PROVIDED BY ARTUR REZNIKOV ''AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL ARTUR REZNIKOV OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the
authors and should not be interpreted as representing official policies, either expressed
or implied, of Artur Reznikov.
"""

__author__ = "Artur Reznikov <areznikov@live.com>"
__version__ = "1.0.1"

import subprocess
import os
import re

class BeanCounters:
    def __init__(self, config, logger, raw_config):
        self.config = config
        self.logger = logger
        self.raw_config = raw_config

    def run(self):
	user_filter = self.raw_config['plugin_beancounters_params'] if self.raw_config.has_key('plugin_beancounters_params') else 'kmemsize,lockedpages,privvmpages,shmpages,numproc,tcpsndbuf,tcprcvbuf,numfile'
	params = user_filter.replace(" ","").split(",")
	command_to_run = 'sudo /bin/cat /proc/user_beancounters'
	command_output = os.popen(command_to_run).readlines()
	return_dict = {}
	for line in command_output:
		split_line=line.split()
		if len(split_line) >=6 and split_line[-6] in params:
			return_dict[split_line[-6]] = split_line[-5]
			return_dict[split_line[-6]+"_limit"] = split_line[-2]
			return_dict[split_line[-6]+"_failcnt"]=split_line[-1]
        return return_dict
